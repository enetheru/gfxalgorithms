Enetheru: Mon 21 Jun 2021 12:29:02
----------------------------------
Added the base structure, Now I have to work on adapting algorithms from other
projects into the structure, figure out exactly what sort of data structure to
draw into, starting with SDL surface data, which I believe is a simple 2d
array, or even contiguous array of data.

I wish to draw

2D
* Lines
* arcs
* besier splines etc
* thick lines
* with and without anti-aliasing
* circles
* primitive shapes
    * circles
    * rectangles
    * AABB
    * polygons
    * enclosed besier
* text
* polygons
* Mathematical plots

And 3D I will not worry about for now as its outside the scop of my immediate
needs.

I guess simply drawing a line should be the start of my journey.

Line
----
* naive algorithm
* Digital Differential Analyzer
* Bresenham's line algorithm
* Xiaolin Wu's line algorithm
* Gupta-Sproull algorithm

As implemented by:
* SDL.
* Cairo


It appears that the Cairo library uses the older autotools configuration
scripts, If I continue my pursuit I might be able to supersede some functions
that it provides, its test suite should be useful.

I have to do a lot of research to figure this out. It appears that cairo does a
similar thing to what I want, except it targets primarily native surfaces
rather than the algorithms themselves. Which means you have to deal with the
vagaries of platforms, which is something I'd rather leave up to a secondary
layer.

When it comes to the end product it might not be possible to avoid the end
result, will have to see. Cairo at least appears to provide some informative
code. it's licenced under LGPL and MPL, So perhaps I can add configure switches
to only include certain licenced code in the final product.

Enetheru: Tue 22 Jun 2021 09:16:37
----------------------------------
A couple of things I would like to do is to template some things, the pixel
writing function, and the colour value. But I think I need more algorithms to
figure out whether that is a good idea.

* pixel format
* set_pixel function

That way I can have strong typing on writing pixel data, and I can use more
than one write function so that I can do effects like burn, multiply, etc.

The hard part is that I need to pass this all to the drawing function as
parameters, maybe, I still need more functions to test with. So for now I will
stick with uint32_t until I can figure out what parameters are needed.

it also doesnt make sense to only draw in colour, but instead have a style
struct that is passed to the drawing function to use whatever it needs to use.
So I can draw in style.

It appears that inside SDL there is something similar, the AALINE macro
specifies a drawing op for the pixel formats, and an endline funciton.
It's just not so exposed to the user I guess because its difficult to manage.

Enetheru: Tue 22 Jun 2021 23:27:44
----------------------------------
Reviewing the SDL code for line drawing, I am slowly drawing conclusions that
its pretty clunky, primarily due to the age of the codebase and that its all
written in C.

I have this idea that most of this should be templated out and inlined.
Each pixel has a pixelformat, which determines what sort of copy function takes
place, and what sort of line is drawn based on the pixel properties, plus the
different line drawing algorithms, etc.

Ugh I need to do some normal work, turns out I don't.

Enetheru: Sun 27 Jun 2021 10:51:57
----------------------------------
The thing I like about this project is that I'm not really inventing anything,
I'm simply re-composing existing things. It gives me a vehicle to learn about
graphics programming which has been something on my mind for 20 years.

Finally found the symbols for the WULINE function, its buried inside macros, I
think I might have to template all these functions to get a complete set of
functions, I can re-combine them manually for the testing purposes.

SDL is so macro heavy, so weird.

Enetheru: Mon 28 Jun 2021 11:03:33
----------------------------------
Down the rabbit hole we go, Alpha blending is needed for the anti-aliasing part
of the wuline function.

So far we have some interesting operations that rely on factors like
* pixel format
* blending
which appear to be the majority of the macro garbage, something I would prefer
to be strongly typed and template or something, there is definitely a thing
here to play with.

Just read the Wikipedia article on alpha composition and it gives a fairly good
treatise on pre-multiplied VS unassociated alpha.

I basically need to re-build what SDL has already done, but in c++ and see if I
can template it all so that it "just works TM".

I want to build up the notion of pixel formats, pixel blend operations, compositing at the pixel
level and have it all just dissappear into templated definitions.

SDL is a big influence but whereas instead of using macro's to achieve the end goals, I would like to
use templates and perhaps concepts moving forward.

I'll have to make the name of the pixel format some type of code so that I can incororate the information of the type.
I want the pixel format to be the type itself like this:
```cpp
struct P_rbga8_a {
    uint32_t data;
};

void decompose( P_rbga8 pixel, uint8_t &r, uint8_t &g, uint8_t &b, uint8_t &a ){
    
}

template<type pixelformat, std::size_t size>
struct Image{
     pixelformat *pixels[size];
};
```

So far in my naive way I want the  name to be `P_<format>_<options>`
because pre-multiplied and non pre-multiplied is a thing, and i'm sure there
are many other things like that.

Enetheru: Mon 28 Jun 2021 20:34:31
----------------------------------
Whilst pulling apart the code for SDL_Pixelformat, it appears that there are
lookup tables for the values of colours for lower bit values, all the way down
to 1 bit as on or off. So we can expand from 8 bits to 0 bits for a colour
component. given I'm dealing right now with singular simple 8 bit rgba colour
then I am thinking I can remove this entire thing.

Well I managed to muddle my way through the macro's and such, have a compiling
version of the wuline algorithm, but I have not yet verified it visually.

Enetheru: Wed 30 Jun 2021 12:42:12
----------------------------------
A pixel format consists of:

* Label (RGBA8, RGB565, etc )
* underlying type (4,8,16,24,32,64,float,etc.
* components
    * Label (RGBAHSVYUVLCM etc)
    * width (bits)
    * shift (position)
* information
    * associated vs unassociated alpha
    * pallet

I want strongly typed pixels based on their pixel format.
I dont need arithmatic, or boolean logic for these pixels as they are the
packed definition, I do need assigment, Perhaps I need a better model still
in my mind, 

What is the general point anyway? So that operations can be made generic based
on templates and still understand what the fuck is going on.

I can run operations like composition of large chunks of data without
needing to know the underlying types, pixel formats or anything.

I need to work on more basic operations before I can tackle this thought
further.

What If I made a visual diagram of Xiaolin Wu's line drawing algorithm in the
spirit of redblob games.. visualised in SDL, I could also re-create it in
javascript. It would prove to be a good article and also allow me to truly
understand it.

As expected each topic is like a rabbit hole, It might be worth my time to
write articles on each algorithm until I understand it.

Enetheru: Wed 07 Jul 2021 11:05:50
----------------------------------
holy shit, all the code for the graphics gems book series is available:

* https://github.com/erich666/GraphicsGems

How good is this, I can finally get my hands on all the goodness i dreamt about
years ago.

Enetheru: Sat 10 Jul 2021 11:37:45
----------------------------------
Arrgh, the more I tried to figure out the Kelvin Thompson anti-aliased line
drawing algorithm the more frustrated I got. It's filled with fixed point math,
what appears to be undefined behaviour due to bit shifting negative values,
global values, macro's, manual memory management,etc. It's just frustratingly
gross. And I still don't really understand the general method, and cannot read
the book to find out. Hmm, quick search and I found a copy of the book haha. OK
time to read what Kelvin has to say about his AALine algorithm, WTF, so
sparse..

Enetheru: Sun 11 Jul 2021 13:09:25
----------------------------------
OK I have a bit of a problem with limits of my wu line drawing algorithm, lets
imagine for a minute that we have an extra large line, we'll use the limits of
whats capable with 32 bit integers. 

A 32 bit singed integer has a size of 2 147 483 647

I need to create a fractional part that fits into a 16 bit integer.
To do this we multiply this by the maximum length of a 16 bit integer before
dividing it.

```
int32_max = 2147483647 
int32_max << 16 = 140737488289792

uint32_max = 4294967295
uint32_max << 16 = 281474976645120

int64_max = 18446744073709551615
```
So it makes sense to promote my numbers to 64 bit unsigned, and divide after
that. I can even use unsigned 32 bit integers for line drawing. But in reality
I doubt that there is memory large enough to require these types of large
numbers, most lines are small. It's just nice to know that mistakes in the
input cannot cause overflow junk.

Enetheru: Sun 11 Jul 2021 13:46:59
----------------------------------
It's so surprising how much depth that can be gone into, its like a crazy
rabbit hole of insanity every time I look at a subject. At some point I have to
give up and just let myself continue. Arrgh, explaining wuline algorithm is
weird, the more i try to explain it the more condensed it becomes. I'm afraid
that I'm losing all the useful information as I condense it.
