//
// Created by enetheru on 21/6/21.
//

#ifndef GFXALGORITHMS_LINE_HPP
#define GFXALGORITHMS_LINE_HPP

#include "image.hpp"

//FIXME Add publishing date, author,

/*! DrawLine
 * @param dst
 * @param x1
 * @param y1
 * @param x2
 * @param y2
 * @param colour
 */

/* Bitmap/Bresenham's line algorithm
 * http://rosettacode.org/wiki/Bitmap/Bresenham%27s_line_algorithm#C.2B.2B
 * Author  20:17,  7 November 2010  by Alegend
 * Revised 20:13, 11 December 2010 by 77.169.85.87 */
void DrawLine_Bresenham_Rosettacode( Image &dst, uint32_t colour, int x1, int y1, int x2, int y2 );

/* Bresenham's line algorithm as implemented by Michael Abrash
 * http://www.phatcode.net/res/224/files/html/ch35/35-03.html
 * http://www.phatcode.net/res/224/files/html/ch35/35-01.html#Heading1 */
/*  Graphics Programming Black Book © 2001 Michael Abrash  */
void DrawLine_Bresenham_Abrash( Image &dst, uint32_t colour, int X0, int Y0, int X1, int Y1 );

/* Bresenham's line algorithm
 * Adapted from the SDL Source */
void DrawLine_Bresenham_SDL( Image &dst, uint32_t colour, int X0, int Y0, int X1, int Y1 );

/* Digital Differential Algorithm adapted from wikipedia */
// https://en.wikipedia.org/wiki/Digital_differential_analyzer_(graphics_algorithm)
void DrawLine_DDA_Wikipedia( Image &dst, uint32_t colour, int x1, int y1, int x2, int y2 );

// https://www.redblobgames.com/grids/line-drawing.html
/* This is not at all representative of RedBlob as the algorithm was intended for educational purposes and has zero optimisation */
void DrawLine_RedBlob_Simple( Image &dst, uint32_t colour, int x1, int y1, int x2, int y2 );

// https://hbfs.wordpress.com/2009/07/28/faster-than-bresenhams-algorithm/
// http://www.edepot.com/algorithm.html
void DrawLine_Bresenham_PoHanLin( Image &dst, uint32_t clr, int x1, int y1, int x2, int y2 );
void DrawLine_Wu_PoHanLin( Image &dst, uint32_t clr, int x1, int y1, int x2, int y2 );
void DrawLine_DDA_PoHanLin( Image &dst, uint32_t clr, int x1, int y1, int x2, int y2 );
void DrawLine_EFLAe_PoHanLin( Image &dst, uint32_t clr, int x, int y, int x2, int y2);

// https://www.geeksforgeeks.org/mid-point-line-generation-algorithm/
void DrawLine_MidPoint_G4G( Image &dst, uint32_t colour, int x1, int y1, int x2, int y2 );

// Gupta-Sproull algorithm

// line drawing using intrinsics? vector math? gpu offloading? AA vs non AA, thick vs thin, alternating stride, etc?

// My idea
void DrawLine_Samuel_Samuel( Image &dst, uint32_t colour , int x1, int y1, int x2, int y2);


#endif //GFXALGORITHMS_LINE_HPP
