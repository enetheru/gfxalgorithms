//
// Created by enetheru on 7/7/21.
//

#ifndef GFXALGORITHMS_AALINE_HPP
#define GFXALGORITHMS_AALINE_HPP

#include "image.hpp"

/* Xiaolin Wu's line algorithm, based on Michael Abrash's implementation
 * adapted from the SDL Source */
// https://unionassets.com/blog/algorithm-brezenhema-and-wu-s-line-299
void DrawAALine_Wu_SDL( Image &dst, uint32_t colour, int X0, int Y0, int X1, int Y1 );

/* TODO Kelvin Thompson's AA line implementation from graphics gems. */
//void DrawAALine_Kelvin_Gems( Image &dst, uint32_t colour, int X1, int Y1, int X2, int Y2 );


#endif //GFXALGORITHMS_AALINE_HPP
