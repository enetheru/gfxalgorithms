//
// Created by enetheru on 5/7/21.
//

#ifndef GFXALGORITHMS_CIRCLE_HPP
#define GFXALGORITHMS_CIRCLE_HPP

// void DrawCircle_Method_Author( Image &dst, int x, int y, int radius, int start, int end, uint32_t colour );

// http://rosettacode.org/wiki/Bitmap/Midpoint_circle_algorithm#C
void DrawCircle_MidPoint_Rosetta( Image &dst, uint32_t colour, int x, int y, int radius);


// https://web.archive.org/web/20120422045142/https://banu.com/blog/7/drawing-circles/
// https://www.gamedev.net/tutorials/_/technical/graphics-programming-and-theory/bresenhams-line-and-circle-algorithms-r767/
/// https://www.professionalcipher.com/2017/08/circle-drawing-using-dda-midpoint-and-bresenham-algorithm.html
// https://www.geeksforgeeks.org/bresenhams-circle-drawing-algorithm/

#endif //GFXALGORITHMS_CIRCLE_HPP
