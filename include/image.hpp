//
// Created by enetheru on 21/6/21.
//

#ifndef GFXALGORITHMS_IMAGE_HPP
#define GFXALGORITHMS_IMAGE_HPP

#include <cstdint>
#include <algorithm>

#include "pixel.hpp"

/* FIXME, I need to define a policy for when the given coordinates are out of bounds of the image buffer, It's not
 *  something I want to have to check all the time for every call, instead it seems like it should be a policy that we
 *  dont explicity check, or have a flag that enables checking for debug purposes. */

struct Image {
    uint32_t *pixels;
    unsigned int pitch;
};

inline void SetPixel( Image &image, uint32_t colour, uint32_t x, uint32_t y ){
    image.pixels[y * image.pitch + x] = colour;
}

inline void SetPixels( Image &image, const uint32_t &colour, uint32_t x, uint32_t y, uint32_t count ){
    auto start = &image.pixels[y * image.pitch + x];
    std::fill( start, start + count, colour);
}

/* Line drawing always assumes left to right, top to bottom */
inline void DrawLineH( Image &image, const uint32_t &colour, uint32_t x, uint32_t y, uint32_t distance ){
    //FIXME ERROR if( x + distance > image.pitch ) would be out of bounds
    auto start = &image.pixels[y * image.pitch + x];
    std::fill( start, start + distance, colour);
}

inline void DrawLineV( Image &image, const uint32_t &colour, int x, int y, int distance ){
    //FIXME ERROR if(y + distance > height) would be out of bounds
    for( int i{0}; i < distance; ++i ){
        image.pixels[ (y+i) * image.pitch + x] = colour;
    }
}

inline void DrawLineD( Image &image, const uint32_t &colour, int x, int y, int distance ){
    //FIXME ERROR if( (y + distance > height) or (x + distance > width) ) would be out of bounds
    for( int i{0}; i < distance; ++i ){
        image.pixels[ (y+i) * image.pitch + (x + i)] = colour;
    }
}

inline void DrawLineD( Image &image, const uint32_t &colour, int x, int y, int stride, int distance ){
    //FIXME ERROR if( (y + distance > height) or (x + distance > width) ) would be out of bounds
    for( int i{0}; i < distance; i+= stride ){
        auto start = &image.pixels[(y+i) * image.pitch + (x + i)];
        std::fill( start, start + stride, colour);
    }
}

inline void BlendPixel( Image &dst, int x, int y,
                        unsigned int r, unsigned int g, unsigned int b, unsigned int a,
                        unsigned int inva ){
    uint32_t &pixel = dst.pixels[ x + y * dst.pitch ];
    auto[_r, _g, _b, _a] = decompose( pixel );

    _r = r + ((_r * inva) >> 8);
    _g = g + ((_g * inva) >> 8);
    _b = b + ((_b * inva) >> 8);
    _a = a + ((_a * inva) >> 8);

    pixel = compose( _r, _g, _b, _a );
}

inline void BlendPixel( Image &dst, int x, int y, uint32_t &pixel ){
    uint32_t &_pixel = dst.pixels[ x + y * dst.pitch ];
    auto[r, g, b, a] = decompose( pixel );
    auto[_r, _g, _b, _a] = decompose( _pixel );
    _r = r + _r * (255 - a);
    _g = g + _g * (255 - a);
    _b = b + _b * (255 - a);
    _a = a + _a * (255 - a);
    _pixel = compose( r, g, b, a );
}

#endif //GFXALGORITHMS_IMAGE_HPP

