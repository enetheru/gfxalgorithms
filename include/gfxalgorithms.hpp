//
// Created by enetheru on 21/6/21.
//

#ifndef GFXALGORITHMS_GFXALGORITHMS_HPP
#define GFXALGORITHMS_GFXALGORITHMS_HPP

#include "pixel.hpp"
#include "line.hpp"
#include "aaline.hpp"
#include "circle.hpp"
// #include "arc.hpp"
// #include "bspline.hpp"
// #include "rectangle.hpp"
// #include "polygon.hpp"

// http://www.svecw.edu.in/Docs%5CCSECGLNotes2013.pdf
// https://www.ferzkopp.net/wordpress/2016/01/02/sdl_gfx-sdl2_gfx/

#endif //GFXALGORITHMS_GFXALGORITHMS_HPP
