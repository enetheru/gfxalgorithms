//
// Created by enetheru on 28/6/21.
//

#ifndef GFXALGORITHMS_PIXEL_HPP
#define GFXALGORITHMS_PIXEL_HPP

#include <cstdint>

/*!
 *
 * @param pixel
 * @return
 */

inline auto decompose( uint32_t pixel ){
    struct binding{uint32_t r,g,b,a;};
    return binding{
        (pixel & 0xFF000000) >> 24,
        (pixel & 0x00FF0000) >> 16,
        (pixel & 0x0000FF00) >> 8,
        (pixel & 0x000000FF)
    };
}

/*!
 *
 * @param r
 * @param g
 * @param b
 * @param a
 * @return
 */

uint32_t compose( unsigned int r, unsigned int g, unsigned int b, unsigned int a );

/*
 * I want strongly typed pixels based on their pixel format.
 * I dont need arithmatic, or boolean logic for these pixels as they are the packed definition
 * I do need assigment
 *
 * Perhaps I need a better model still in my mind,
 */

/*
struct Component{
    char label;
    uint8_t width;
    uint8_t shift;
    uint8_t struct_padding;
};

template<typename uType, std::size_t nComponents>
struct PixelFormat{
    const char *label;
    uType type;
    Component components[nComponents];
    bool pAlpha;
    bool pallet;
};

const static auto rgba8_p = PixelFormat<uint32_t, 4>{
    .label="RGBA8_A",
    .components={
            {.label='R',.width=4,.shift=24},
            {.label='G',.width=4,.shift=16},
            {.label='B',.width=4,.shift= 8},
            {.label='A',.width=4,.shift= 0}},
            .pAlpha=true,
            .pallet=false
};
 */
#endif //GFXALGORITHMS_PIXEL_HPP
