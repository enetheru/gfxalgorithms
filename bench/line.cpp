//
// Created by enetheru on 22/6/21.
//
#include <benchmark/benchmark.h>
#include <gfxalgorithms.hpp>
#include "data_coords_circle_512.hpp"

static void BM_Line_Bresenham_Rosetta( benchmark::State &state ){
    uint32_t pixels[512 * 512];
    Image image {pixels, 512};

    for( auto _ : state ){
        for( int i = 0; i < 100; ++i ){
            DrawLine_Bresenham_Rosettacode( image, 0xFFFFFFFF, 255, 255, coords_circle_512[i], coords_circle_512[i+1] );
        }
    }
}
BENCHMARK( BM_Line_Bresenham_Rosetta )->Unit(benchmark::kMillisecond);

static void BM_Line_Bresenham_Abrash( benchmark::State &state ){
    uint32_t pixels[512 * 512];
    Image image{};
    image.pixels = pixels;
    image.pitch = 512;

    for( auto _ : state ){
        for( int i = 0; i < 100; ++i ){
            DrawLine_Bresenham_Abrash( image, 0xFFFFFFFF, 255, 255, coords_circle_512[i], coords_circle_512[i+1] );
        }
    }
}
BENCHMARK( BM_Line_Bresenham_Abrash )->Unit(benchmark::kMillisecond);

static void BM_Line_Bresenham_SDL( benchmark::State &state ){
    uint32_t pixels[512 * 512];
    Image image{pixels,512};

    for( auto _ : state ){
        for( int i = 0; i < 100; ++i ){
            DrawLine_Bresenham_SDL( image, 0xFFFFFFFF, 255, 255, coords_circle_512[i], coords_circle_512[i+1] );
        }
    }
}
BENCHMARK( BM_Line_Bresenham_SDL )->Unit(benchmark::kMillisecond);

static void BM_Line_Wu_SDL( benchmark::State &state ){
    uint32_t pixels[512 * 512];
    Image image{pixels,512};

    for( auto _ : state ){
        for( int i = 0; i < 100; ++i ){
            DrawAALine_Wu_SDL( image, 0xFFFFFFFF , 255, 255, coords_circle_512[ i ], coords_circle_512[ i + 1 ]);
        }
    }
}
BENCHMARK( BM_Line_Wu_SDL )->Unit(benchmark::kMillisecond);

static void BM_Line_DDA_Wikipedia( benchmark::State &state ){
    uint32_t pixels[512 * 512];
    Image image{pixels,512};

    for( auto _ : state ){
        for( int i = 0; i < 100; ++i ){
            DrawLine_DDA_Wikipedia( image, 0xFFFFFFFF, 255, 255, coords_circle_512[i], coords_circle_512[i+1] );
        }
    }
}
BENCHMARK( BM_Line_DDA_Wikipedia )->Unit(benchmark::kMillisecond);

static void BM_Line_RedBlob_Simple( benchmark::State &state ){
    uint32_t pixels[512 * 512];
    Image image{pixels,512};

    for( auto _ : state ){
        for( int i = 0; i < 100; ++i ){
            DrawLine_RedBlob_Simple( image, 0xFFFFFFFF , 255, 255, coords_circle_512[i], coords_circle_512[i+1]);
        }
    }
}
BENCHMARK( BM_Line_RedBlob_Simple )->Unit(benchmark::kMillisecond);

static void BM_Line_Bresenham_PoHanLin( benchmark::State &state ){
    uint32_t pixels[512 * 512];
    Image image{pixels,512};

    for( auto _ : state ){
        for( int i = 0; i < 100; ++i ){
            DrawLine_Bresenham_PoHanLin( image, 0xFFFFFFFF , 255, 255, coords_circle_512[i], coords_circle_512[i+1]);
        }
    }
}
BENCHMARK( BM_Line_Bresenham_PoHanLin )->Unit(benchmark::kMillisecond);

static void BM_Line_Wu_PoHanLin( benchmark::State &state ){
    uint32_t pixels[512 * 512];
    Image image{pixels,512};

    for( auto _ : state ){
        for( int i = 0; i < 100; ++i ){
            DrawLine_Wu_PoHanLin( image, 0xFFFFFFFF, 255, 255, coords_circle_512[i], coords_circle_512[i+1] );
        }
    }
}
BENCHMARK( BM_Line_Wu_PoHanLin )->Unit(benchmark::kMillisecond);

static void BM_Line_DDA_PoHanLin( benchmark::State &state ){
    uint32_t pixels[512 * 512];
    Image image{pixels,512};

    for( auto _ : state ){
        for( int i = 0; i < 100; ++i ){
            DrawLine_DDA_PoHanLin( image, 0xFFFFFFFF, 255, 255, coords_circle_512[i], coords_circle_512[i+1] );
        }
    }
}
BENCHMARK( BM_Line_DDA_PoHanLin )->Unit(benchmark::kMillisecond);

static void BM_Line_EFLAe_PoHanLin( benchmark::State &state ){
    uint32_t pixels[512 * 512];
    Image image{pixels,512};

    for( auto _ : state ){
        for( int i = 0; i < 100; ++i ){
            DrawLine_EFLAe_PoHanLin( image, 0xFFFFFFFF , 255, 255, coords_circle_512[i], coords_circle_512[i+1]);
        }
    }
}
BENCHMARK( BM_Line_EFLAe_PoHanLin )->Unit(benchmark::kMillisecond);

static void BM_Line_MidPoint_G4G( benchmark::State &state ){
    uint32_t pixels[512 * 512];
    Image image{pixels,512};

    for( auto _ : state ){
        for( int i = 0; i < 100; ++i ){
            DrawLine_MidPoint_G4G( image, 0xFFFFFFFF, 255, 255, coords_circle_512[i], coords_circle_512[i+1] );
        }
    }
}
BENCHMARK( BM_Line_MidPoint_G4G )->Unit(benchmark::kMillisecond);
