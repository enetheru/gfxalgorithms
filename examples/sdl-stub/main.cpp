//
// Created by enetheru on 21/6/21.
//

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <fmt/format.h>

#include <gfxalgorithms.hpp>

const char *window_title = "Window Title";
bool debug = true;
bool run = true;

int
main( [[maybe_unused]] int argc, [[maybe_unused]] char **argv )
{

    if( SDL_Init( SDL_INIT_VIDEO ) != 0 ){
        fmt::print( "SDL_Init Error: {}\n", SDL_GetError() );
        exit( 1 );
    }

    if( TTF_Init() != 0 ){
        fmt::print( "TTF_Init Error: {}\n", TTF_GetError() );
        SDL_Quit();
        exit( 1 );
    }

    SDL_version compile_version;
    const SDL_version *link_version = TTF_Linked_Version();
    SDL_TTF_VERSION( &compile_version )
    fmt::print("compiled with SDL_ttf version: {}.{}.{}\n",
               compile_version.major,
               compile_version.minor,
               compile_version.patch);
    fmt::print("running with SDL_ttf version: {}.{}.{}\n",
               link_version->major,
               link_version->minor,
               link_version->patch);

    SDL_Window *window = SDL_CreateWindow( window_title, 0, 0, 640, 480, SDL_WINDOW_UTILITY | SDL_WINDOW_RESIZABLE );
    if( not window ){
        fmt::print( "SDL_CreateWindow Error: {}\n", SDL_GetError() );
        SDL_Quit();
        exit( 1 );
    }

    SDL_Renderer *renderer = SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED );//| SDL_RENDERER_PRESENTVSYNC );
    if( not renderer ){
        fmt::print( "SDL_CreateRenderer Error: {}\n", SDL_GetError() );
        SDL_Quit();
        exit( 1 );
    }

    TTF_Font *font = TTF_OpenFont( "res/DejaVuSansMono.ttf", 20 );
    if( not font ){
        fmt::print( "TTF_OpenFont Error: {}\n", TTF_GetError() );
        SDL_Quit();
        exit( 1 );
    }

    while( run ){
        //copy SDL events into director
        SDL_Event ev;
        while( SDL_PollEvent( &ev ) ){
            switch( ev.type ){
                case SDL_KEYDOWN:
                    switch( ev.key.keysym.sym ){
                        case SDLK_ESCAPE:
                            run = false;
                            break;
                        case SDLK_d:
                            debug = !debug;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        //clear the renderer
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
        SDL_RenderClear( renderer );

        if( debug ){
            /* render debug info */
        }

        //Update the screen
        SDL_UpdateWindowSurface( window );
        SDL_RenderPresent( renderer );
    }
    SDL_Quit();
    return 0;
}
