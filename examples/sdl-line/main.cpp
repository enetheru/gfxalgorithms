//
// Created by enetheru on 21/6/21.
//

#include <vector>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <fmt/format.h>

#include <gfxalgorithms.hpp>
#include <data_coords_circle_512.hpp>

const char *window_title = "Window Title";
bool debug = true;
bool run = true;
int width = 512;
int height = 512;

using name_algo_pair = std::pair< const char *, void (*)( Image &, const uint32_t, int, int, int, int ) >;

std::vector<name_algo_pair> line_algorithms = {
        {"Bresenham Rosetta", DrawLine_Bresenham_Rosettacode},
        {"Bresenham Abrash", DrawLine_Bresenham_Abrash},
        {"Bresenham SDL", DrawLine_Bresenham_SDL},
        {"Bresenham PoHanLin", DrawLine_Bresenham_PoHanLin },
        {"DDA Wikipedia", DrawLine_DDA_Wikipedia},
        {"DDA PoHanLin", DrawLine_DDA_PoHanLin },
        {"RedBLob Simple", DrawLine_RedBlob_Simple },
        {"Wu SDL", DrawAALine_Wu_SDL},
        {"Wu PoHanLin", DrawLine_Wu_PoHanLin },
        {"EFLAe PoHanLin", DrawLine_EFLAe_PoHanLin },
        {"MidPoint G4G", DrawLine_MidPoint_G4G },
};

SDL_Texture *re_draw_lines( SDL_Renderer *renderer, unsigned int algorithm){
    SDL_Surface *surface = SDL_CreateRGBSurfaceWithFormat(0, width, height, 0, SDL_PIXELFORMAT_RGBA32 );

    /* Draw The line */
    Image image{reinterpret_cast<uint32_t*>(surface->pixels), static_cast<unsigned int>(surface->pitch / 4)};
    for( int i = 0; i < 360 * 2; i += 8){
        line_algorithms[algorithm].second(image,0xFFFFFFFF, 255,255,coords_circle_512[i],coords_circle_512[i+1]);
    }
    SDL_Texture *texture = SDL_CreateTextureFromSurface( renderer, surface );
    SDL_FreeSurface( surface );
    return texture;
}

auto re_draw_label( SDL_Renderer *renderer, TTF_Font *font, unsigned int algorithm ){
    struct bindings{ SDL_Texture *texture; SDL_Rect rect; };
    auto colour = SDL_Color{255,255,255,255};
    SDL_Surface *surface = TTF_RenderText_Solid(font, line_algorithms[algorithm].first, colour );
    SDL_Texture *texture = SDL_CreateTextureFromSurface( renderer, surface );
    SDL_Rect rect{0,0,surface->w,surface->h};
    SDL_FreeSurface( surface );
    return bindings{ texture, rect };
}

int
main( [[maybe_unused]] int argc, [[maybe_unused]] char **argv )
{

    if( SDL_Init( SDL_INIT_VIDEO ) != 0 ){
        fmt::print( "SDL_Init Error: {}\n", SDL_GetError() );
        exit( 1 );
    }

    if( TTF_Init() != 0 ){
        fmt::print( "TTF_Init Error: {}\n", TTF_GetError() );
        SDL_Quit();
        exit( 1 );
    }

    SDL_version compile_version;
    const SDL_version *link_version = TTF_Linked_Version();
    SDL_TTF_VERSION( &compile_version )
    fmt::print("compiled with SDL_ttf version: {}.{}.{}\n",
               compile_version.major,
               compile_version.minor,
               compile_version.patch);
    fmt::print("running with SDL_ttf version: {}.{}.{}\n",
               link_version->major,
               link_version->minor,
               link_version->patch);

    SDL_Window *window = SDL_CreateWindow( window_title, 0, 0, width, height, SDL_WINDOW_UTILITY | SDL_WINDOW_RESIZABLE );
    if( not window ){
        fmt::print( "SDL_CreateWindow Error: {}\n", SDL_GetError() );
        SDL_Quit();
        exit( 1 );
    }

    SDL_Renderer *renderer = SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED );//| SDL_RENDERER_PRESENTVSYNC );
    if( not renderer ){
        fmt::print( "SDL_CreateRenderer Error: {}\n", SDL_GetError() );
        SDL_Quit();
        exit( 1 );
    }

    TTF_Font *font = TTF_OpenFont( "res/DejaVuSansMono.ttf", 20 );
    if( not font ){
        fmt::print( "TTF_OpenFont Error: {}\n", TTF_GetError() );
        SDL_Quit();
        exit( 1 );
    }

    unsigned int algorithm = 0;
    SDL_Texture *lines = re_draw_lines( renderer, algorithm );
    auto [label, label_rect] = re_draw_label( renderer, font, algorithm );

    while( run ){
        //copy SDL events into director
        SDL_Event ev;
        while( SDL_PollEvent( &ev ) ){
            switch( ev.type ){
                case SDL_KEYDOWN:
                    switch( ev.key.keysym.sym ){
                        case SDLK_ESCAPE:
                            run = false;
                            break;
                        case SDLK_d:
                            debug = !debug;
                            break;
                        default:
                            break;
                        case SDLK_SPACE:
                            if( ( ++algorithm) == line_algorithms.size() ) algorithm = 0;
                            SDL_DestroyTexture( lines );
                            lines = re_draw_lines( renderer, algorithm );
                            SDL_DestroyTexture( label );
                            auto [tlabel,tlabel_rect] = re_draw_label( renderer, font, algorithm );
                            label = tlabel; label_rect = tlabel_rect;
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        //clear the renderer
        SDL_SetRenderDrawColor(renderer, 255, 0, 0, SDL_ALPHA_OPAQUE);
        SDL_RenderClear( renderer );

        SDL_SetTextureBlendMode( lines, SDL_BLENDMODE_NONE );
        SDL_RenderCopy( renderer, lines, nullptr, nullptr );
        SDL_RenderCopy( renderer, label, nullptr, &label_rect );



        if( debug ){
            /* render debug info */
        }

        //Update the screen
        SDL_UpdateWindowSurface( window );
        SDL_RenderPresent( renderer );
    }
    SDL_DestroyTexture( lines );
    SDL_Quit();
    return 0;
}
