//
// Created by enetheru on 21/6/21.
//
#include <gfxalgorithms.hpp>
#include <utility>

int main(){

    uint32_t pixels[512 * 512];
    Image image{ pixels, 512 };

    DrawLine_Bresenham_Rosettacode(image,0xFFFFFFFF, 256,256,0,0 );
    DrawLine_Bresenham_Abrash(image,0xFFFFFFFF, 256,256,0,0 );
    DrawLine_Bresenham_SDL(image,0xFFFFFFFF, 256,256,0,0 );
    DrawAALine_Wu_SDL( image, 0xFFFFFFFF, 256, 256, 0, 0 );
    return 0;
}