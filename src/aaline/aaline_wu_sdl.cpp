//
// Created by enetheru on 22/6/21.
//
#include <cstdint>
#include <algorithm>

#include "aaline.hpp"
#include "image.hpp"
#include "pixel.hpp"

/* Xiaolin Wu's line algorithm, based on Michael Abrash's implementation */
/* Xiaolin Wu's line algorithm, based on Michael Abrash's implementation */
void DrawAALine_Wu_SDL( Image &dst, const uint32_t colour, int X0, int Y0, int X1, int Y1 ){
    auto [r,g,b,a] = decompose( colour );

    int DeltaX, DeltaY, XDir;
    unsigned int pr, pg, pb, pa, inva;

    /* Draw the initial and final pixels which are always exactly intersected by
       the line and so need no weighting */
    SetPixel( dst, colour, X0, Y0 );
    SetPixel( dst, colour, X1, Y1 );

    /* Make sure the line runs top to bottom */
    if( Y0 > Y1 ){
        std::swap( Y0, Y1);
        std::swap( X0, X1);
    }

    DeltaY = Y1 - Y0;
    DeltaX = X1 - X0;
    if( DeltaX >= 0 ){
        XDir = 1;
    } else{
        XDir = -1;
        DeltaX = abs(DeltaX); /* make DeltaX positive */
    }

    /* line is not horizontal, diagonal, or vertical */
    if( Y0 == Y1 ) return DrawLineH( dst, colour, X0 < X1 ? X0 : X1, Y0, DeltaX );
    if( X0 == X1 ) return DrawLineV( dst, colour, X0, Y0, DeltaY );
    if( DeltaX == DeltaY) return DrawLineD( dst, colour, X0 < X1 ? X0 : X1, Y0, DeltaX );

    uint16_t ErrorAdj, ErrorAcc = 0; /* initialize the line error accumulator to 0 */
    uint16_t ErrorAccTemp, Weighting;

    /* Is this an X-major or Y-major line? */
    if( DeltaY > DeltaX ){
        /* Y-major line; calculate 16-bit fixed-point fractional part of a
          pixel that X advances each time Y advances 1 pixel, truncating the
          result so that we won't overrun the endpoint along the X axis */
        ErrorAdj = (static_cast<unsigned long long>(DeltaX) << 16) / static_cast<unsigned long long int>(DeltaY);
        /* Draw all pixels other than the first and last */

        while( --DeltaY ){
            ErrorAccTemp = ErrorAcc;   /* remember currrent accumulated error */
            ErrorAcc += ErrorAdj;      /* calculate error for next pixel */
            if( ErrorAcc <= ErrorAccTemp ){
                /* The error accumulator turned over, so advance the X coord */
                X0 += XDir;
            }
            Y0++; /* Y-major, so always advance Y */
            /* The IntensityBits most significant bits of ErrorAcc give us the
             intensity weighting for this pixel, and the complement of the
             weighting for the paired pixel */
            Weighting = ErrorAcc >> 8;
            {
                pa = (a * (Weighting ^ 0x00FF) ) >>8;
                pr = (r * pa ) >> 8;
                pg = (g * pa ) >> 8;
                pb = (b * pa ) >> 8;
                inva = (pa ^ 0x000000FF);
                BlendPixel( dst, X0, Y0, pr, pg, pb, pa, inva );

            }
            {
                pa = (a * Weighting ) >> 8;
                pr = (r * pa ) >> 8;
                pg = (g * pa ) >> 8;
                pb = (b * pa ) >> 8;
                inva = (pa ^ 0xFF);
                BlendPixel( dst, X0 + XDir, Y0, pr, pg, pb, pa, inva );

            }
        }
    } else{
        /* X-major line; calculate 16-bit fixed-point fractional part of a
           pixel that Y advances each time X advances 1 pixel, truncating the
           result to avoid overrunning the endpoint along the X axis */
        ErrorAdj = (static_cast<unsigned long long>(DeltaY) << 16) / static_cast<unsigned long long>(DeltaX);
        /* Draw all pixels other than the first and last */
        while( --DeltaX ){
            ErrorAccTemp = ErrorAcc;   /* remember currrent accumulated error */
            ErrorAcc += ErrorAdj;      /* calculate error for next pixel */
            if( ErrorAcc <= ErrorAccTemp ){
                /* The error accumulator overflowed, so advance the Y coord */
                Y0++;
            }
            X0 += XDir; /* X-major, so always advance X */
            /* The IntensityBits most significant bits of ErrorAcc give us the
              intensity weighting for this pixel, and the complement of the
              weighting for the paired pixel */
            Weighting = ErrorAcc >> 8; //Clamp range to 0-255
            { //construct  the primary pre-multiplied pixel
                pa = (a * (Weighting ^ 0xFF) ) >> 8;
                pr = (r * pa ) >> 8;
                pg = (g * pa ) >> 8;
                pb = (b * pa ) >> 8;
                inva = (pa ^ 0xFF);
                BlendPixel( dst, X0, Y0, pr, pg, pb, pa, inva );

            }
            { //construct pre-multiplied pixel
                pa = (a * Weighting ) >> 8;
                pr = (r * pa ) >> 8;
                pg = (g * pa ) >> 8;
                pb = (b * pa ) >> 8;
                inva = (pa ^ 0xFF);
                BlendPixel( dst, X0, Y0 + 1, pr, pg, pb, pa, inva );
            }
        }
    }
}
