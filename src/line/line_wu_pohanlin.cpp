//
// Created by enetheru on 5/7/21.
//

#include "image.hpp"

// http://www.edepot.com/algorithm.html
void DrawLine_Wu_PoHanLin( Image &dst, uint32_t clr, int x0, int y0, int x1, int y1) {
    //int pix = color.getRGB();
    int dy = y1 - y0;
    int dx = x1 - x0;
    int stepx, stepy;

    if (dy < 0) { dy = -dy;  stepy = -1; } else { stepy = 1; }
    if (dx < 0) { dx = -dx;  stepx = -1; } else { stepx = 1; }

    SetPixel( dst, clr, x0, y0);
    SetPixel( dst, clr, x1, y1);
    if (dx > dy) {
        int length = (dx - 1) >> 2;
        int extras = (dx - 1) & 3;
        int incr2 = (dy << 2) - (dx << 1);
        if (incr2 < 0) {
            int c = dy << 1;
            int incr1 = c << 1;
            int d =  incr1 - dx;
            for (int i = 0; i < length; i++) {
                x0 += stepx;
                x1 -= stepx;
                if (d < 0) {                                               // Pattern:
                    SetPixel( dst, clr, x0, y0);                          //
                    SetPixel( dst, clr, x0 += stepx, y0);                 //  x o o
                    SetPixel( dst, clr, x1, y1);                          //
                    SetPixel( dst, clr, x1 -= stepx, y1);
                    d += incr1;
                } else {
                    if (d < c) {                                           // Pattern:
                        SetPixel( dst, clr, x0, y0);                      //      o
                        SetPixel( dst, clr, x0 += stepx, y0 += stepy);    //  x o
                        SetPixel( dst, clr, x1, y1);                      //
                        SetPixel( dst, clr, x1 -= stepx, y1 -= stepy);
                    } else {
                        SetPixel( dst, clr, x0, y0 += stepy);             // Pattern:
                        SetPixel( dst, clr, x0 += stepx, y0);             //    o o 
                        SetPixel( dst, clr, x1, y1 -= stepy);             //  x
                        SetPixel( dst, clr, x1 -= stepx, y1);             //
                    }
                    d += incr2;
                }
            }
            if (extras > 0) {
                if (d < 0) {
                    SetPixel( dst, clr, x0 += stepx, y0);
                    if (extras > 1) SetPixel( dst, clr, x0 += stepx, y0);
                    if (extras > 2) SetPixel( dst, clr, x1 -= stepx, y1);
                } else
                    if (d < c) {
                        SetPixel( dst, clr, x0 += stepx, y0);
                        if (extras > 1) SetPixel( dst, clr, x0 += stepx, y0 += stepy);
                        if (extras > 2) SetPixel( dst, clr, x1 -= stepx, y1);
                    } else {
                        SetPixel( dst, clr, x0 += stepx, y0 += stepy);
                        if (extras > 1) SetPixel( dst, clr, x0 += stepx, y0);
                        if (extras > 2) SetPixel( dst, clr, x1 -= stepx, y1 -= stepy);
                    }
            }
        } else {
            int c = (dy - dx) << 1;
            int incr1 = c << 1;
            int d =  incr1 + dx;
            for (int i = 0; i < length; i++) {
                x0 += stepx;
                x1 -= stepx;
                if (d > 0) {
                    SetPixel( dst, clr, x0, y0 += stepy);                      // Pattern:
                    SetPixel( dst, clr, x0 += stepx, y0 += stepy);             //      o
                    SetPixel( dst, clr, x1, y1 -= stepy);                      //    o
                    SetPixel( dst, clr, x1 -= stepx, y1 -= stepy);		        //  x
                    d += incr1;
                } else {
                    if (d < c) {
                        SetPixel( dst, clr, x0, y0);                           // Pattern:
                        SetPixel( dst, clr, x0 += stepx, y0 += stepy);         //      o
                        SetPixel( dst, clr, x1, y1);                           //  x o
                        SetPixel( dst, clr, x1 -= stepx, y1 -= stepy);         //
                    } else {
                        SetPixel( dst, clr, x0, y0 += stepy);                  // Pattern:
                        SetPixel( dst, clr, x0 += stepx, y0);                  //    o o
                        SetPixel( dst, clr, x1, y1 -= stepy);                  //  x
                        SetPixel( dst, clr, x1 -= stepx, y1);                  //
                    }
                    d += incr2;
                }
            }
            if (extras > 0) {
                if (d > 0) {
                    SetPixel( dst, clr, x0 += stepx, y0 += stepy);
                    if (extras > 1) SetPixel( dst, clr, x0 += stepx, y0 += stepy);
                    if (extras > 2) SetPixel( dst, clr, x1 -= stepx, y1 -= stepy);
                } else
                    if (d < c) {
                        SetPixel( dst, clr, x0 += stepx, y0);
                        if (extras > 1) SetPixel( dst, clr, x0 += stepx, y0 += stepy);
                        if (extras > 2) SetPixel( dst, clr, x1 -= stepx, y1);
                    } else {
                        SetPixel( dst, clr, x0 += stepx, y0 += stepy);
                        if (extras > 1) SetPixel( dst, clr, x0 += stepx, y0);
                        if (extras > 2) {
                            if (d > c)
                                SetPixel( dst, clr, x1 -= stepx, y1 -= stepy);
                            else
                                SetPixel( dst, clr, x1 -= stepx, y1);
                        }
                    }
            }
        }
    } else {
        int length = (dy - 1) >> 2;
        int extras = (dy - 1) & 3;
        int incr2 = (dx << 2) - (dy << 1);
        if (incr2 < 0) {
            int c = dx << 1;
            int incr1 = c << 1;
            int d =  incr1 - dy;
            for (int i = 0; i < length; i++) {
                y0 += stepy;
                y1 -= stepy;
                if (d < 0) {
                    SetPixel( dst, clr, x0, y0);
                    SetPixel( dst, clr, x0, y0 += stepy);
                    SetPixel( dst, clr, x1, y1);
                    SetPixel( dst, clr, x1, y1 -= stepy);
                    d += incr1;
                } else {
                    if (d < c) {
                        SetPixel( dst, clr, x0, y0);
                        SetPixel( dst, clr, x0 += stepx, y0 += stepy);
                        SetPixel( dst, clr, x1, y1);
                        SetPixel( dst, clr, x1 -= stepx, y1 -= stepy);
                    } else {
                        SetPixel( dst, clr, x0 += stepx, y0);
                        SetPixel( dst, clr, x0, y0 += stepy);
                        SetPixel( dst, clr, x1 -= stepx, y1);
                        SetPixel( dst, clr, x1, y1 -= stepy);
                    }
                    d += incr2;
                }
            }
            if (extras > 0) {
                if (d < 0) {
                    SetPixel( dst, clr, x0, y0 += stepy);
                    if (extras > 1) SetPixel( dst, clr, x0, y0 += stepy);
                    if (extras > 2) SetPixel( dst, clr, x1, y1 -= stepy);
                } else
                    if (d < c) {
                        SetPixel( dst, clr, stepx, y0 += stepy);
                        if (extras > 1) SetPixel( dst, clr, x0 += stepx, y0 += stepy);
                        if (extras > 2) SetPixel( dst, clr, x1, y1 -= stepy);
                    } else {
                        SetPixel( dst, clr, x0 += stepx, y0 += stepy);
                        if (extras > 1) SetPixel( dst, clr, x0, y0 += stepy);
                        if (extras > 2) SetPixel( dst, clr, x1 -= stepx, y1 -= stepy);
                    }
            }
        } else {
            int c = (dx - dy) << 1;
            int incr1 = c << 1;
            int d =  incr1 + dy;
            for (int i = 0; i < length; i++) {
                y0 += stepy;
                y1 -= stepy;
                if (d > 0) {
                    SetPixel( dst, clr, x0 += stepx, y0);
                    SetPixel( dst, clr, x0 += stepx, y0 += stepy);
                    SetPixel( dst, clr, x1 -= stepy, y1);
                    SetPixel( dst, clr, x1 -= stepx, y1 -= stepy);
                    d += incr1;
                } else {
                    if (d < c) {
                        SetPixel( dst, clr, x0, y0);
                        SetPixel( dst, clr, x0 += stepx, y0 += stepy);
                        SetPixel( dst, clr, x1, y1);
                        SetPixel( dst, clr, x1 -= stepx, y1 -= stepy);
                    } else {
                        SetPixel( dst, clr, x0 += stepx, y0);
                        SetPixel( dst, clr, x0, y0 += stepy);
                        SetPixel( dst, clr, x1 -= stepx, y1);
                        SetPixel( dst, clr, x1, y1 -= stepy);
                    }
                    d += incr2;
                }
            }
            if (extras > 0) {
                if (d > 0) {
                    SetPixel( dst, clr, x0 += stepx, y0 += stepy);
                    if (extras > 1) SetPixel( dst, clr, x0 += stepx, y0 += stepy);
                    if (extras > 2) SetPixel( dst, clr, x1 -= stepx, y1 -= stepy);
                } else
                    if (d < c) {
                        SetPixel( dst, clr, x0, y0 += stepy);
                        if (extras > 1) SetPixel( dst, clr, x0 += stepx, y0 += stepy);
                        if (extras > 2) SetPixel( dst, clr, x1, y1 -= stepy);
                    } else {
                        SetPixel( dst, clr, x0 += stepx, y0 += stepy);
                        if (extras > 1) SetPixel( dst, clr, x0, y0 += stepy);
                        if (extras > 2) {
                            if (d > c)
                                SetPixel( dst, clr, x1 -= stepx, y1 -= stepy);
                            else
                                SetPixel( dst, clr, x1, y1 -= stepy);
                        }
                    }
            }
        }
    }
}