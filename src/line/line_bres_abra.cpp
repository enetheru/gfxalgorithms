//
// Created by enetheru on 21/6/21.
//

#include <utility>

#include "line.hpp"

/* Bresenham's line algorithm as implemented by Michael Abrash
 * http://www.phatcode.net/res/224/files/html/ch35/35-03.html
 * Draws a line in octant 0 or 3 ( |DeltaX| >= DeltaY ).
 * unsigned int X0, Y0;           coordinates of start of the line
 * unsigned int DeltaX, DeltaY;   length of the line (both > 0)
 * int XDirection;                1 if line is drawn left to right,
 *                                -1 if drawn right to left
 */

static void Octant0( Image &image,
              int X0, int Y0,
              int DeltaX, int DeltaY,
              int XDirection,
              const uint32_t &colour ){
    int DeltaYx2;
    int DeltaYx2MinusDeltaXx2;
    int ErrorTerm;

    /* Set up initial error term and values used inside drawing loop */
    DeltaYx2 = DeltaY * 2;
    DeltaYx2MinusDeltaXx2 = DeltaYx2 - DeltaX * 2;
    ErrorTerm = DeltaYx2 - (int)DeltaX;

    /* Draw the line */
    SetPixel( image, colour, X0, Y0 ); /* draw the first pixel */
    while( DeltaX-- ) {
        /* See if it’s time to advance the Y coordinate */
        if( ErrorTerm >= 0 ){
            /* Advance the Y coordinate & adjust the error term back down */
            Y0++;
            ErrorTerm += DeltaYx2MinusDeltaXx2;
        } else {
            /* Add to the error term */
            ErrorTerm += DeltaYx2;
        }
        X0 += XDirection;          /* advance the X coordinate */
        SetPixel( image, colour, X0, Y0 ); /* draw a pixel */
    }
}


/*
 * Draws a line in octant 1 or 2 ( |DeltaX| < DeltaY ).
 * unsigned int X0, Y0;          coordinates of start of the line
 * unsigned int DeltaX, DeltaY;  length of the line (both > 0)
 * int XDirection;               1 if line is drawn left to right,
 *                               -1 if drawn right to left
 */
static void Octant1( Image &image,
              int X0, int Y0,
              int DeltaX, int DeltaY,
              int XDirection,
              const uint32_t &colour ){
    int DeltaXx2;
    int DeltaXx2MinusDeltaYx2;
    int ErrorTerm;

    /* Set up initial error term and values used inside drawing loop */
    DeltaXx2 = DeltaX * 2;
    DeltaXx2MinusDeltaYx2 = DeltaXx2 - (int)(DeltaY * 2);
    ErrorTerm = DeltaXx2 - (int)DeltaY;

    SetPixel( image, colour, X0, Y0 ); /* draw the first pixel */
    while( DeltaY-- ) {
        /* See if it’s time to advance the X coordinate */
        if( ErrorTerm >= 0 ){
            /* Advance the X coordinate & adjust the error term back down */
            X0 += XDirection;
            ErrorTerm += DeltaXx2MinusDeltaYx2;
        } else{
            /* Add to the error term */
            ErrorTerm += DeltaXx2;
        }
        Y0++;                   /* advance the Y coordinate */
        SetPixel( image, colour, X0, Y0 ); /* draw a pixel */
    }
}

/*
 * Draws a line on the EGA or VGA.
 * int X0, Y0;    coordinates of one end of the line
 * int X1, Y1;    coordinates of the other end of the line
 * char Color;    color to draw line in
 */
void DrawLine_Bresenham_Abrash( Image &dst,
                                const uint32_t colour ,
                                int X0, int Y0,
                                int X1, int Y1){
    int DeltaX, DeltaY;

    /* Save half the line-drawing cases by swapping Y0 with Y1
        and X0 with X1 if Y0 is greater than Y1. As a result, DeltaY
        is always > 0, and only the octant 0-3 cases need to be
        handled. */
    if( Y0 > Y1 ){
        std::swap(Y0,Y1);
        std::swap( X0,X1);
    }

    /* Handle as four separate cases, for the four octants in which Y1 is greater than Y0 */
    DeltaX = X1 - X0;    /* calculate the length of the line in each coordinate */
    DeltaY = Y1 - Y0;
    if( DeltaX > 0 ){
        if( DeltaX > DeltaY ){
            Octant0( dst, X0, Y0, DeltaX, DeltaY, 1, colour );
        } else{
            Octant1( dst, X0, Y0, DeltaX, DeltaY, 1, colour );
        }
    } else {
        DeltaX = -DeltaX;             /* absolute value of DeltaX */
        if( DeltaX > DeltaY ){
            Octant0( dst, X0, Y0, DeltaX, DeltaY, -1, colour );
        } else{
            Octant1( dst, X0, Y0, DeltaX, DeltaY, -1, colour );
        }
    }
}