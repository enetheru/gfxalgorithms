//
// Created by enetheru on 3/7/21.
//

#include "image.hpp"

void DrawLine_DDA_Wikipedia( Image &dst, const uint32_t colour, int x1, int y1, int x2, int y2 ){
    float x, y, dx, dy, step;

    dx = (x2 - x1);
    dy = (y2 - y1);

    if( abs(dx) >= abs(dy) )
        step = abs( dx );
    else
        step = abs( dy );
    dx = dx / step;
    dy = dy / step;
    x = x1;
    y = y1;
    for(int i = 1; i <= step; ++i){
        SetPixel( dst, colour, x, y);
        x = x + dx;
        y = y + dy;
    }
}