//
// Created by enetheru on 22/6/21.
//

#include <cmath>

#include "line.hpp"
#include "image.hpp"

/* Bresenham's line algorithm */
void DrawLine_Bresenham_SDL( Image &dst, const uint32_t colour, int X0, int Y0, int X1, int Y1)
{
    int i, deltax, deltay, numpixels;
    int d, dinc1, dinc2;
    int x, xinc1, xinc2;
    int y, yinc1, yinc2;
    deltax = abs( X1 - X0);
    deltay = abs( Y1 - Y0);
    if (deltax >= deltay) {
        numpixels = deltax + 1;
        d = (2 * deltay) - deltax;
        dinc1 = deltay * 2;
        dinc2 = (deltay - deltax) * 2;
        xinc1 = 1;
        xinc2 = 1;
        yinc1 = 0;
        yinc2 = 1;
    } else {
        numpixels = deltay + 1;
        d = (2 * deltax) - deltay;
        dinc1 = deltax * 2;
        dinc2 = (deltax - deltay) * 2;
        xinc1 = 0;
        xinc2 = 1;
        yinc1 = 1;
        yinc2 = 1;
    }
    if ( X0 > X1) {
        xinc1 = -xinc1;
        xinc2 = -xinc2;
    }
    if ( Y0 > Y1) {
        yinc1 = -yinc1;
        yinc2 = -yinc2;
    }
    x = X0;
    y = Y0;
    for (i = 0; i < numpixels; ++i) {
        SetPixel( dst, colour, x, y);
        if (d < 0) {
            d += dinc1;
            x += xinc1;
            y += yinc1;
        } else {
            d += dinc2;
            x += xinc2;
            y += yinc2;
        }
    }
}