//
// Created by enetheru on 3/7/21.
//

#include <algorithm>
#include <cmath>

#include "line.hpp"

struct point{
    float x,y;
};

static float diagonal_distance( point p0, point p1 ){
    float dx = p1.x - p0.x, dy = p1.y - p0.y;
    return std::max( std::abs(dx), std::abs(dy) );
}

static auto round_point( point p ) {
    return point{ std::round(p.x), std::round(p.y) };
}

float lerp( float start, float end, float t ){
    return start + t * (end-start);
}

static auto lerp_point( point p0, point p1, float t) {
    return point{ lerp( p0.x, p1.x, t ), lerp( p0.y, p1.y, t) };
}

void DrawLine_RedBlob_Simple( Image &dst, const uint32_t colour, int x1, int y1, int x2, int y2 ){
    float N = diagonal_distance({x1, y1}, {x2, y2} );
    for( int step = 0; step <= N; step++) {
        float t = N == 0 ? 0.0 : step / N;
        point p = round_point( lerp_point({x1, y1}, {x2, y2}, t) );
        SetPixel(dst, colour, p.x, p.y );
    }
}
