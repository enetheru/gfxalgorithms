//
// Created by enetheru on 5/7/21.
//

#include "image.hpp"

// http://www.edepot.com/algorithm.html
void DrawLine_DDA_PoHanLin( Image &dst, uint32_t clr, int x1, int y1, int x2, int y2 ){
    int length,i;
    double x,y;
    double xincrement;
    double yincrement;

    length = abs(x2 - x1);
    if (abs(y2 - y1) > length) length = abs(y2 - y1);
    xincrement = (double)(x2 - x1)/(double)length;
    yincrement = (double)(y2 - y1)/(double)length;
    x = x1 + 0.5;
    y = y1 + 0.5;
    for (i = 1; i<= length;++i) {
        SetPixel( dst, clr, (int)x, (int)y );
        x = x + xincrement;
        y = y + yincrement;
    }
}