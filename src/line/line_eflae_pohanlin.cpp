//
// Created by enetheru on 5/7/21.
//
#include "image.hpp"

// http://www.edepot.com/algorithm.html
// THE EXTREMELY FAST LINE ALGORITHM Variation E (Addition fixed point precalc)
void DrawLine_EFLAe_PoHanLin( Image &dst, uint32_t clr, int x, int y, int x2, int y2) {
    bool yLonger=false;
    int shortLen=y2-y;
    int longLen=x2-x;
    if (abs(shortLen)>abs(longLen)) {
        int swap=shortLen;
        shortLen=longLen;
        longLen=swap;
        yLonger=true;
    }
    int decInc;
    if (longLen==0) decInc=0;
    else decInc = (shortLen << 16) / longLen;

    if (yLonger) {
        if (longLen>0) {
            longLen+=y;
            for (int j=0x8000+(x<<16);y<=longLen;++y) {
                SetPixel( dst,clr, j >> 16,y);
                j+=decInc;
            }
            return;
        }
        longLen+=y;
        for (int j=0x8000+(x<<16);y>=longLen;--y) {
            SetPixel( dst,clr, j >> 16,y);
            j-=decInc;
        }
        return;
    }

    if (longLen>0) {
        longLen+=x;
        for (int j=0x8000+(y<<16);x<=longLen;++x) {
            SetPixel( dst,clr, x,j >> 16);
            j+=decInc;
        }
        return;
    }
    longLen+=x;
    for (int j=0x8000+(y<<16);x>=longLen;--x) {
        SetPixel( dst,clr, x,j >> 16);
        j-=decInc;
    }

}