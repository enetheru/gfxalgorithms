//
// Created by enetheru on 21/6/21.
//
#include <cmath>
#include <utility>

#include "line.hpp"

// Bresenham's line algorithm
// http://rosettacode.org/wiki/Bitmap/Bresenham%27s_line_algorithm#C.2B.2B
void DrawLine_Bresenham_Rosettacode( Image &dst, const uint32_t colour , int x1, int y1, int x2, int y2)
{
    const bool steep = (fabsf(y2 - y1) > fabsf(x2 - x1));
    if(steep){
        std::swap(x1, y1);
        std::swap(x2, y2);
    }

    if(x1 > x2)
    {
        std::swap(x1, x2);
        std::swap(y1, y2);
    }

    const float dx = x2 - x1;
    const float dy = fabsf(y2 - y1);

    float error = dx / 2.0f;
    const int ystep = (y1 < y2) ? 1 : -1;
    int y = (int)y1;

    const int maxX = (int)x2;

    for(int x=(int)x1; x<=maxX; x++)
    {
        if(steep)
        {
            SetPixel( dst, colour, y, x);
        }
        else
        {
            SetPixel( dst, colour, x, y);
        }

        error -= dy;
        if(error < 0)
        {
            y += ystep;
            error += dx;
        }
    }
}