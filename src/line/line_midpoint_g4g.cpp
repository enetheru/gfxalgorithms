//
// Created by enetheru on 5/7/21.
//

#include "image.hpp"

// Adapted from articl from Geeks4Geeks who credit: Shivam Pradhan (anuj_charm)
// https://www.geeksforgeeks.org/mid-point-line-generation-algorithm/
// midPoint function for line generation
void DrawLine_MidPoint_G4G( Image &dst, uint32_t colour , int X1, int Y1, int X2, int Y2){

    if( X1 > X2){
        std::swap(X1, X2);
        std::swap(Y1, Y2);
    }
    // calculate dx & dy
    int dx = X2 - X1;
    int dy = Y2 - Y1;

    if( dy <= dx ){ /* X-Major Line */
        // initial value of decision parameter d
        int d = dy - (dx / 2);
        int x = X1, y = Y1;

        // Plot initial given point
        SetPixel( dst, colour, x, y );

        // iterate through value of X
        while( x < X2 ){
            x++;

            // E or East is chosen
            if( d < 0 )
                d = d + dy;

                // NE or North East is chosen
            else{
                d += (dy - dx);
                y++;
            }

            // Plot intermediate points
            SetPixel( dst, colour, x, y );

        }
    } else if( dx < dy ){ /* Y-Major Line */
        // initial value of decision parameter d
        int d = dx - (dy / 2);
        int x = X1, y = Y1;

        // Plot initial given point
        SetPixel( dst, colour, x, y );


        // iterate through value of X
        while( y < Y2 ){
            y++;

            // E or East is chosen
            if( d < 0 )
                d = d + dx;

                // NE or North East is chosen
                // NE or North East is chosen
            else{
                d += (dx - dy);
                x++;
            }

            // Plot intermediate points
            SetPixel( dst, colour, x, y );
        }
    }
}