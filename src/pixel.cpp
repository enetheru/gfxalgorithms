//
// Created by enetheru on 2/7/21.
//

#include "pixel.hpp"

uint32_t compose( unsigned int r, unsigned int g, unsigned int b, unsigned int a ){
    return r << 24 | g << 16 | b << 8 | a;
}
