//
// Created by enetheru on 6/7/21.
//

#include "image.hpp"

// http://rosettacode.org/wiki/Bitmap/Midpoint_circle_algorithm#C
void DrawCircle_MidPoint_Rosetta( Image &dst, uint32_t colour, int x0, int y0, int radius){
    int f = 1 - radius;
    int ddF_x = 0;
    int ddF_y = -2 * radius;
    int x = 0;
    int y = radius;

    SetPixel( dst, colour, x0, y0 + radius );
    SetPixel( dst, colour, x0, y0 - radius );
    SetPixel( dst, colour, x0 + radius, y0 );
    SetPixel( dst, colour, x0 - radius, y0 );

    while( x < y ){
        if( f >= 0 ){
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x + 1;
        SetPixel( dst, colour, x0 + x, y0 + y );
        SetPixel( dst, colour, x0 - x, y0 + y );
        SetPixel( dst, colour, x0 + x, y0 - y );
        SetPixel( dst, colour, x0 - x, y0 - y );
        SetPixel( dst, colour, x0 + y, y0 + x );
        SetPixel( dst, colour, x0 - y, y0 + x );
        SetPixel( dst, colour, x0 + y, y0 - x );
        SetPixel( dst, colour, x0 - y, y0 - x );
    }
}
